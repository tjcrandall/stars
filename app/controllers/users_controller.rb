class UsersController < ApplicationController
  before_filter :authenticate_user!
  before_filter :correct_user?, :except => [:index]

  def index
    @users = User.all
  end

  def show
    @user = User.find(params[:id])
  end
  def add_star
	@current = User.find(params[:id])
	@current.increment!(:star)
	redirect_to user_url
  end
end
