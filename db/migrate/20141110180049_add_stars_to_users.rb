class AddStarsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :star, :integer
  end
end
